﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_wk2_ex3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Press 1 to convert from Kilometers to Miles, Press 2 to convert from Miles to Kilometers");
            var input = int.Parse(Console.ReadLine());
            switch (input)
            {
                case (1):
                    Console.WriteLine("You have chosen to convert from Kilometers to Miles");
                    Console.WriteLine("Please Enters the amount of Kilometers you would like converted to Miles");
                    var kilo2mile = int.Parse(Console.ReadLine());
                    Console.WriteLine($"{kilo2mile} kilometers is {kilo2mile * 0.62} miles");
                    break;
                case (2):
                    Console.WriteLine("You have chosen to convert from Miles to Kilometers");
                    Console.WriteLine("Please Enters the amount of Miles you would like converted to Kilometers");
                    var mile2kilo = int.Parse(Console.ReadLine());
                    Console.WriteLine($"{mile2kilo} miles is {mile2kilo * 1.62} kilometers");
                    break;

            }
        }
    }
}
