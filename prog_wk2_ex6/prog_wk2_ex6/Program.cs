﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_wk2_ex6
{
    class Program
    {
        static void Main(string[] args)
        {
            var i = 0;
            while (i < 5)
            {
                i++;
                Console.WriteLine($"This is line number {i}");
            }
        }
    }
}
