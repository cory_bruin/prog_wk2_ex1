﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_wk2_ex7
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a number");
            var input = int.Parse(Console.ReadLine());
            var counter = 20;
            do
            {
                input++;
                Console.WriteLine($"This is line {input}");
            } while (input < counter);
            
        }
    }
}
