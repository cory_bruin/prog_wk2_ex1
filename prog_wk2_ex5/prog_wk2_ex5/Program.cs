﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_wk2_ex5
{
    class Program
    {
        static void Main(string[] args)
        {
            int score = 0;
            Start:
            Random rnd = new Random();
            int Number1 = rnd.Next(1, 50);
            int Number2 = rnd.Next(1, 50);
        
            Console.WriteLine($"What is {Number1} + {Number2}");
            int answer = Number1 + Number2;
            var response = int.Parse(Console.ReadLine());
            if (response == answer)
            {
                score++;
                Console.WriteLine($"Congratulations - that is correct. Score is now {score}");
                goto Start;
            }
            else
            {
                Console.WriteLine($"That is Incorrect - Score is still: {score}");
                goto Start;
            }
        }
    }
}
