﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_wk2_ex2
{
    class Program
    {
        static void Main(string[] args)
        {
            var colour = Console.ReadLine();

            switch (colour)
            {
                case "blue":
                    Console.WriteLine("You picked blue - The sky is blue");
                    break;
                case "red":
                    Console.WriteLine("You picked red - Apples can be red");
                    break;
                default:
                    Console.WriteLine("You picked the wrong colour");
                    break;            
            }
        }
    }
}
