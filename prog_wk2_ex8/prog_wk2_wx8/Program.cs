﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_wk2_wx8
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Is the sky blue? true or false");
            var input = bool.Parse(Console.ReadLine());
            var result = "";
            if (input)
            {
                result = "Yes the sky is blue";
            }
            else
            {
                result = "You are incorrect - the sky is blue";
            }
            Console.WriteLine(result);
        }
    }
}
