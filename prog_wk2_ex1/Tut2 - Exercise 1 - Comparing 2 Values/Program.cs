﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tut2___Exercise_1___Comparing_2_Values
{
    class Program
    {
        static void Main(string[] args)
        {
            var number1 = 3;
            var number2 = 5;
            if (number1 > number2)
            {
                Console.WriteLine("Number1 is bigger than Number 2");
            }
            else { Console.WriteLine("Number1 is smaller or equal to Number2"); }

        }
    }
}
