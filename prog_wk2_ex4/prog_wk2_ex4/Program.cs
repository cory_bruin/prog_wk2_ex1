﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_wk2_ex4
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 0; i < 5; i++) 
            {
                var a = i + 1;
                Console.WriteLine($"This is line number {a}");
            }
        }
    }
}
